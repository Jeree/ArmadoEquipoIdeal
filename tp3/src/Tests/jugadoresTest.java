package el11ideal.Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import el11ideal.Jugador;
import el11ideal.Jugadores;

@SuppressWarnings("static-access")
public class jugadoresTest 
{

	@Test
	public void maxTest() 
	{
		Jugadores lista = cargarJugadores () ;

		Jugador mejor = lista.mejorJugador();
		assertEquals( 12 , mejor.getCoeficiente());	
	}

	@Test
	public void ordenarTest () 
	{
		Jugadores lista = cargarJugadores () ;

		Jugador j1 = lista._Jugadores.get(0);
		Jugador j2 = lista._Jugadores.get(1);
		Jugador j3 = lista._Jugadores.get(2);
		Jugador j4 = lista._Jugadores.get(3);
		Jugador j5 = lista._Jugadores.get(4);

		lista.ordenar();

		Assert.iguales(new Jugador [] {j1,j3,j4,j5,j2 }  , lista._Jugadores);
	}

	@Test
	public void agregarJugadorTest ( )
	{
		Jugadores lista = cargarJugadores() ;
		lista.agregarJugador("nuevo jugador", 1, 2, 0, 8, 3, "Argentina", "Volante izquierdo");
		assertTrue(lista.getSize() == 6 ); 
	}

	@Test (expected=IllegalArgumentException.class)
	public void agregarJugadorNoPermitidoTest () 
	{
		Jugadores lista = cargarJugadores() ;

		Jugador j = new Jugador ("A", 15 , 20, 5, 6, 2, "Argentina", "Arquero") ;

		lista.agregarJugador(j);
	}

	@Test
	public void agregarPosicionTest () 
	{
		Jugadores jugadores = cargarJugadores() ;
		jugadores.agreagarPosicion("D", "Central derecho");
		int cantidadPosiciones = jugadores._Jugadores.get(3).getPosicion().size(); 
		assertEquals(2, cantidadPosiciones);
	}

	@Test
	public void agregarArgumentoInvalidoTest () 
	{
		Jugadores jugadores = cargarJugadores() ;
		Jugador nuevo = new Jugador ( "Z" , -1 , 5, 5, -2, -7, "Chile", "Arquero"  );
		jugadores.agregarJugador(nuevo);
	}

	public Jugadores cargarJugadores () 
	{
		Jugador j1 = new Jugador ("A", 15, 20, 5, 6, 2, "Argentina", "Arquero") ;
		Jugador j2 = new Jugador ("B", 10, 20, 8, 4, 2, "Brasil", "Lateral izquierdo" ) ;
		Jugador j3 = new Jugador ("C", 15, 12, 7, 8, 4, "Espa�a", "Central izquierdo") ;
		Jugador j4 = new Jugador ("D", 12, 8, 10, 10, 5, "Argentina", "Central derecho") ;
		Jugador j5 = new Jugador ("E", 14, 25, 15, 20, 10, "Brasil", "Lateral derecho") ;

		Jugadores lista = new Jugadores ( ) ;

		lista.agregarJugador(j1);
		lista.agregarJugador(j2);
		lista.agregarJugador(j3);
		lista.agregarJugador(j4);
		lista.agregarJugador(j5);

		return lista ;
	}
}


