package el11ideal.Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import el11ideal.Jugador;

public class Assert
	{
		public static void iguales ( Jugador [] esperado , ArrayList <Jugador> obtenido) 
		{
			assertEquals ( esperado.length , obtenido.size());
			
			for ( int i = 0 ; i<esperado.length;i++)
			{
				assertTrue( esperado [i] == obtenido.get(i));
			}
		}
	}