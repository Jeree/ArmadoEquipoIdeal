package el11ideal.Tests;

import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import org.junit.Test;
import el11ideal.ArmadoEquipoIdeal;
import el11ideal.Jugador;
import el11ideal.Jugadores;

@SuppressWarnings("static-access")
public class ArmadoEquipoIdealTest 
{
	@Test
	public void armadoCorrectoTest() 
	{
		Jugadores jugadores = cargarJugadores () ;

		Jugador j12 = jugadores._Jugadores.get(12-1);
		Jugador j2 = jugadores._Jugadores.get(2-1);
		Jugador j3 = jugadores._Jugadores.get(3-1);
		Jugador j4 = jugadores._Jugadores.get(4-1);
		Jugador j5 = jugadores._Jugadores.get(5-1);
		Jugador j11 = jugadores._Jugadores.get(11-1);
		Jugador j14 = jugadores._Jugadores.get(14-1);
		Jugador j7 = jugadores._Jugadores.get(7-1);
		Jugador j13 = jugadores._Jugadores.get(13-1);
		Jugador j9 = jugadores._Jugadores.get(9-1);
		Jugador j10 = jugadores._Jugadores.get(10-1);

		ArrayList<Jugador> lista = jugadores._Jugadores;

		lista = ArmadoEquipoIdeal.armadoEquipo(lista);

		Assert.iguales(new Jugador [] {j12,j2,j3,j4,j5,j11,j14,j7,j13,j9,j10}, lista);
	}

	@Test ( expected = IllegalArgumentException.class)
	public void pocisionesVaciasTest() 
	{
		Jugadores jugadores = cargarJugadores () ;
		ArrayList <Jugador> listaNueva = jugadores._Jugadores;
		listaNueva.remove(1);
		ArmadoEquipoIdeal.armadoEquipo(listaNueva);	
	}

	@Test 
	public void excedeMaxSinGolesTest () 
	{
		Jugadores jugadores= cargarJugadores () ;
		ArrayList<Jugador> lista = jugadores.getListaJugadores();
		lista.set(0, new Jugador ("D", 0, 0, 0, 10, 5, "Suecia", "Central derecho")) ;
		ArmadoEquipoIdeal.armadoEquipo(lista);
		assertTrue(ArmadoEquipoIdeal.excedeCondicion(lista));
	}

	@Test 
	public void excedeMaxTarjetas () 
	{
		Jugadores jugadores= cargarJugadores () ;
		ArrayList<Jugador> lista = jugadores.getListaJugadores();
		lista.set(3, new Jugador ("D", 0, 0, 1, 10, 5, "Suecia", "Central derecho")) ;
		lista.set(7, new Jugador("H", 3, 0, 2, 8, 5, "Alemania", "Puntero izquierdo")) ;
		lista.set(1, new Jugador("B", 2, 0, 1, 5, 5, "Argentina", "Lateral izquierdo")) ;
		lista.set(2, new Jugador("C", 0, 0, 3, 8, 5, "Argentina", "Central izquierdo")) ;
		lista.set(5, new Jugador("F", 6, 0, 1, 6, 5, "Brasil", "Volante central")) ;
		ArmadoEquipoIdeal.armadoEquipo(lista);
		assertTrue(ArmadoEquipoIdeal.excedeCondicion(lista));
	}

	@Test 
	public void excedeMaxMismoPaisTest () 
	{
		Jugadores jugadores= cargarJugadores () ;
		ArrayList<Jugador> lista = jugadores.getListaJugadores();
		lista.set(3, new Jugador ("D", 0, 0, 1, 10, 5, "Argentina", "Central derecho")) ;
		ArmadoEquipoIdeal.armadoEquipo(lista);
		assertTrue(ArmadoEquipoIdeal.excedeCondicion(lista));
	}

	public Jugadores cargarJugadores () 
	{
		Jugadores lista = new Jugadores ( ) ;

		Jugador j1 = new Jugador ("A", 0, 0, 0, 7, 5, "Argentina", "Arquero") ;
		lista.agregarJugador(j1);
		j1.addPosicionAsignada("Arquero");

		Jugador j2 = new Jugador ("B", 2, 0, 0, 5, 5, "Argentina", "Lateral izquierdo" ) ;
		lista.agregarJugador(j2);
		j2.addPosicionAsignada("Lateral izquierdo");

		Jugador j3 = new Jugador ("C", 0, 0, 0, 8, 5, "Argentina", "Central izquierdo") ;
		lista.agregarJugador(j3);
		j3.addPosicionAsignada("Central izquierdo");

		Jugador j4 = new Jugador ("D", 1, 0, 0, 10, 5, "Suecia", "Central derecho") ;
		lista.agregarJugador(j4);
		j4.addPosicionAsignada("Central derecho");

		Jugador j5 = new Jugador ("E", 4, 0, 0, 7, 5, "Brasil", "Lateral derecho") ;
		lista.agregarJugador(j5);
		j5.addPosicionAsignada("Lateral derecho");

		Jugador j6 = new Jugador ("F", 0, 0, 0, 6, 5, "Brasil", "Volante central") ;
		lista.agregarJugador(j6);
		j6.addPosicionAsignada("Volante central");

		Jugador j7 = new Jugador ("G", 1, 0, 0, 4, 5, "Brasil", "Volante derecho" ) ;
		lista.agregarJugador(j7);
		j7.addPosicionAsignada("Volante derecho");

		Jugador j8 = new Jugador ("H", 3, 0, 0, 8, 5, "Alemania", "Puntero izquierdo") ;
		lista.agregarJugador(j8);
		j8.addPosicionAsignada("Puntero izquierdo");

		Jugador j9 = new Jugador ("I", 4, 0, 0, 5, 5, "Espa�a", "Centro delantero") ;
		lista.agregarJugador(j9);
		j9.addPosicionAsignada("Centro delantero");

		Jugador j10 = new Jugador ("J", 0, 0, 0, 2, 5, "Espa�a", "Puntero derecho") ;
		lista.agregarJugador(j10);
		j10.addPosicionAsignada("Centro delantero");

		Jugador j11 = new Jugador ("K", 2, 0, 0, 6, 5, "Espa�a", "Volante izquierdo") ;
		lista.agregarJugador(j11);
		j11.addPosicionAsignada("Volante derecho");

		Jugador j12 = new Jugador ("L", 2, 0, 0, 4, 5, "Camerun", "Arquero" ) ;
		lista.agregarJugador(j12);
		j12.addPosicionAsignada("Arquero");

		Jugador j13 = new Jugador ("M", 8, 0, 0, 8, 5, "Rusia", "Puntero izquierdo") ;
		lista.agregarJugador(j13);
		j13.addPosicionAsignada("Puntero izquierdo");

		Jugador j14 = new Jugador ("N", 3, 0, 0, 6, 5, "Rusia", "Volante central") ;
		lista.agregarJugador(j14);
		j14.addPosicionAsignada("Volante central");

		Jugador j15 = new Jugador ("o", 0, 0, 0, 4, 5, "Rusia", "Lateral derecho") ;
		lista.agregarJugador(j15);
		j15.addPosicionAsignada("Lateral derecho");

		return lista ;
	}
}