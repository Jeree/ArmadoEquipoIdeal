package el11ideal.interfazGrafica;
import javax.swing.JFrame;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.Font;
import java.awt.Image;
import javax.swing.JTextField;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.SwingConstants;
import el11ideal.ArmadoEquipoIdeal;
import el11ideal.Jugador;
import el11ideal.Jugadores;
import java.awt.Color;
import javax.swing.JButton;

@SuppressWarnings("static-access")
public class InterfazMostrarListaIdeal extends JFrame implements ArmadoEquipoIdeal
{
	private static final long serialVersionUID = -2193667596025166295L; 
	public JPanel contentPane;
	public boolean botonAceptar= false;
	private JTextField txtArquero;
	private JTextField txtLateralDerecho;
	private JTextField txtCentralDerecho;
	private JTextField txtCentralIzquierdo;
	private JTextField txtLateralIzquierdo;
	private JTextField txtVolanteCentral;
	private JTextField txtVolanteDerecho;
	private JTextField txtVolanteIzquierdo;
	private JTextField txtCentrodelantero;
	private JTextField txtPunteroIzquierdo;
	private JTextField txtPunteroDerecho;
	private JButton btnInicio;
	private JButton btnCargar; 
	private JButton Salir;
	ArrayList <Jugador> listaArmada = new ArrayList <Jugador > (); 

	public InterfazMostrarListaIdeal( Jugadores listaJugadores ) 
	{
		if ( ArmadoEquipoIdeal.excedeCondicion(listaJugadores.getListaJugadores()))
		{
			JOptionPane.showMessageDialog(null, "Tienes jugadores que no cumplen con las condiciones minimas");
		}

		//ArrayList<Jugador> listaIdeal = armarEquipo ( cargarJugadores() );
		ArrayList<Jugador> listaIdeal = armarEquipo ( listaJugadores );

		
		getContentPane().setBackground(Color.WHITE);
		setTitle("Mundial Rusia 2018");
		setIconImage(Toolkit.getDefaultToolkit().getImage(InterfazMostrarListaIdeal.class.getResource("/Imagenes/copa mundial.jpg")));
		getContentPane().setFont(new Font("Source Sans Pro Black", Font.PLAIN, 21));
		getContentPane().setLayout(null);
		setVisible(false);
		setSize(1000,600);

		JLabel lblBuscarLaMina = new JLabel("El equipo ideal es");
		lblBuscarLaMina.setToolTipText("");
		lblBuscarLaMina.setFont(new Font("Source Sans Pro Black", Font.PLAIN, 43));
		lblBuscarLaMina.setBounds(314, 0, 379, 44);
		getContentPane().add(lblBuscarLaMina);

		txtArquero = new JTextField( extraerJugador(listaIdeal, "Arquero"));
		txtArquero.setEditable(false);
		txtArquero.setHorizontalAlignment(SwingConstants.CENTER);
		txtArquero.setBounds(63, 214, 143, 36);
		getContentPane().add(txtArquero);
		txtArquero.setColumns(10);

		txtLateralDerecho = new JTextField();
		txtLateralDerecho.setText(extraerJugador(listaIdeal,"Lateral derecho"));
		txtLateralDerecho.setHorizontalAlignment(SwingConstants.CENTER);
		txtLateralDerecho.setEditable(false);
		txtLateralDerecho.setColumns(10);
		txtLateralDerecho.setBounds(224, 351, 154, 36);
		getContentPane().add(txtLateralDerecho);

		txtCentralDerecho = new JTextField(extraerJugador(listaIdeal,"Central derecho"));
		txtCentralDerecho.setHorizontalAlignment(SwingConstants.CENTER);
		txtCentralDerecho.setEditable(false);
		txtCentralDerecho.setColumns(10);
		txtCentralDerecho.setBounds(133, 283, 154, 36);
		getContentPane().add(txtCentralDerecho);

		txtCentralIzquierdo = new JTextField(extraerJugador(listaIdeal,"Central izquierdo"));
		txtCentralIzquierdo.setHorizontalAlignment(SwingConstants.CENTER);
		txtCentralIzquierdo.setEditable(false);
		txtCentralIzquierdo.setColumns(10);
		txtCentralIzquierdo.setBounds(133, 157, 154, 36);
		getContentPane().add(txtCentralIzquierdo);

		txtLateralIzquierdo = new JTextField(extraerJugador(listaIdeal,"Lateral izquierdo"));
		txtLateralIzquierdo.setHorizontalAlignment(SwingConstants.CENTER);
		txtLateralIzquierdo.setEditable(false);
		txtLateralIzquierdo.setColumns(10);
		txtLateralIzquierdo.setBounds(215, 82, 154, 36);
		getContentPane().add(txtLateralIzquierdo);

		txtVolanteCentral = new JTextField(extraerJugador(listaIdeal,"Volante central"));
		txtVolanteCentral.setHorizontalAlignment(SwingConstants.CENTER);
		txtVolanteCentral.setEditable(false);
		txtVolanteCentral.setColumns(10);
		txtVolanteCentral.setBounds(396, 226, 154, 36);
		getContentPane().add(txtVolanteCentral);

		txtVolanteDerecho = new JTextField(extraerJugador(listaIdeal,"Volante derecho"));
		txtVolanteDerecho.setHorizontalAlignment(SwingConstants.CENTER);
		txtVolanteDerecho.setEditable(false);
		txtVolanteDerecho.setColumns(10);
		txtVolanteDerecho.setBounds(465, 335, 154, 36);
		getContentPane().add(txtVolanteDerecho);

		txtVolanteIzquierdo = new JTextField(extraerJugador(listaIdeal,"Volante izquierdo"));
		txtVolanteIzquierdo.setHorizontalAlignment(SwingConstants.CENTER);
		txtVolanteIzquierdo.setEditable(false);
		txtVolanteIzquierdo.setColumns(10);
		txtVolanteIzquierdo.setBounds(454, 82, 154, 36);
		getContentPane().add(txtVolanteIzquierdo);

		txtCentrodelantero = new JTextField(extraerJugador(listaIdeal,"Centro delantero"));
		txtCentrodelantero.setHorizontalAlignment(SwingConstants.CENTER);
		txtCentrodelantero.setEditable(false);
		txtCentrodelantero.setColumns(10);
		txtCentrodelantero.setBounds(656, 226, 154, 36);
		getContentPane().add(txtCentrodelantero);

		txtPunteroIzquierdo = new JTextField(extraerJugador(listaIdeal,"Puntero izquierdo"));
		txtPunteroIzquierdo.setHorizontalAlignment(SwingConstants.CENTER);
		txtPunteroIzquierdo.setEditable(false);
		txtPunteroIzquierdo.setColumns(10);
		txtPunteroIzquierdo.setBounds(681, 131, 154, 36);
		getContentPane().add(txtPunteroIzquierdo);

		txtPunteroDerecho = new JTextField(extraerJugador(listaIdeal,"Puntero derecho"));
		txtPunteroDerecho.setHorizontalAlignment(SwingConstants.CENTER);
		txtPunteroDerecho.setEditable(false);
		txtPunteroDerecho.setColumns(10);
		txtPunteroDerecho.setBounds(754, 291, 154, 36);
		getContentPane().add(txtPunteroDerecho);

		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setBounds(0, 40, 974, 392);
		ImageIcon imagen1= new ImageIcon(getClass().getResource("/Imagenes/Campo de soccer.jpg"));
		Icon fondo = new ImageIcon(imagen1.getImage().getScaledInstance(lblNewLabel.getWidth(), lblNewLabel.getHeight(), Image.SCALE_DEFAULT));
		lblNewLabel.setIcon(fondo);
		this.repaint();
		getContentPane().add(lblNewLabel);	

		btnInicio = new JButton("Inicio");
		btnInicio.setBounds(10, 506, 131, 44);
		btnInicio.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				new InterfazInicio().setVisible(true);
				dispose();
			}
		});

		getContentPane().add(btnInicio);

		btnCargar = new JButton("Cargar");
		btnCargar.setBounds(428, 506, 131, 44);
		btnCargar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				new InterfazCargaDeDatos().setVisible(true);
				dispose();
			}
		});
		getContentPane().add(btnCargar);

		Salir = new JButton("Salir");
		Salir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				System.exit(0);
			}
		});
		Salir.setBounds(843, 506, 131, 44);
		getContentPane().add(Salir);
		
		JLabel lblNewLabel_1 = new JLabel("El coeficiente total del equipo es: " + ArmadoEquipoIdeal.coeficienteTotal(listaArmada));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 40));
		lblNewLabel_1.setBounds(26, 443, 948, 52);
		getContentPane().add(lblNewLabel_1);

	}

	private ArrayList <Jugador> armarEquipo( Jugadores l ) 
	{ 
		try
		{
			listaArmada =  ArmadoEquipoIdeal.armadoEquipo ( l._Jugadores ) ;
		}
		catch ( Exception ex )
		{
			JOptionPane.showMessageDialog(null, "El equipo no se pudo armar ya que faltan jugades");
		}
		return listaArmada ;	
	}

	private String extraerJugador (ArrayList <Jugador> jugadores , String posicion )
	{
		String nombreJugador = "vacio" ;
		for ( Jugador j : jugadores)
		{
			if ( j.getPosicionAsignada() == posicion)
				nombreJugador = j.getNombre() ;
		}		
		return nombreJugador;
	}
	public Jugadores cargarJugadores () 
	{
		Jugadores lista = new Jugadores ( ) ;

		Jugador j1 = new Jugador ("A", 0, 0, 0, 7, 5, "Argentina", "Arquero") ;
		lista.agregarJugador(j1);
		j1.addPosicionAsignada("Arquero");

		Jugador j2 = new Jugador ("B", 2, 0, 0, 5, 5, "Argentina", "Lateral izquierdo" ) ;
		lista.agregarJugador(j2);
		j2.addPosicionAsignada("Lateral izquierdo");

		Jugador j3 = new Jugador ("C", 0, 0, 0, 8, 5, "Argentina", "Central izquierdo") ;
		lista.agregarJugador(j3);
		j3.addPosicionAsignada("Central izquierdo");

		Jugador j4 = new Jugador ("D", 1, 0, 0, 10, 5, "Suecia", "Central derecho") ;
		lista.agregarJugador(j4);
		j4.addPosicionAsignada("Central derecho");

		Jugador j5 = new Jugador ("E", 4, 0, 0, 7, 5, "Brasil", "Lateral derecho") ;
		lista.agregarJugador(j5);
		j5.addPosicionAsignada("Lateral derecho");

		Jugador j6 = new Jugador ("F", 0, 0, 0, 6, 5, "Brasil", "Volante central") ;
		lista.agregarJugador(j6);
		j6.addPosicionAsignada("Volante central");

		Jugador j7 = new Jugador ("G", 1, 0, 0, 4, 5, "Brasil", "Volante derecho" ) ;
		lista.agregarJugador(j7);
		j7.addPosicionAsignada("Volante derecho");

		Jugador j8 = new Jugador ("H", 3, 0, 0, 8, 5, "Alemania", "Puntero izquierdo") ;
		lista.agregarJugador(j8);
		j8.addPosicionAsignada("Puntero izquierdo");

		Jugador j9 = new Jugador ("I", 4, 0, 0, 5, 5, "Espa�a", "Centro delantero") ;
		lista.agregarJugador(j9);
		j9.addPosicionAsignada("Centro delantero");

		Jugador j10 = new Jugador ("J", 0, 0, 0, 2, 5, "Espa�a", "Puntero derecho") ;
		lista.agregarJugador(j10);
		j10.addPosicionAsignada("Centro delantero");

		Jugador j11 = new Jugador ("K", 2, 0, 0, 6, 5, "Espa�a", "Volante izquierdo") ;
		lista.agregarJugador(j11);
		j11.addPosicionAsignada("Volante derecho");

		Jugador j12 = new Jugador ("L", 2, 0, 0, 4, 5, "Camerun", "Arquero" ) ;
		lista.agregarJugador(j12);
		j12.addPosicionAsignada("Arquero");

		Jugador j13 = new Jugador ("M", 8, 0, 0, 8, 5, "Rusia", "Puntero izquierdo") ;
		lista.agregarJugador(j13);
		j13.addPosicionAsignada("Puntero izquierdo");

		Jugador j14 = new Jugador ("N", 3, 0, 0, 6, 5, "Rusia", "Volante central") ;
		lista.agregarJugador(j14);
		j14.addPosicionAsignada("Volante central");

		Jugador j15 = new Jugador ("o", 0, 0, 0, 4, 5, "Rusia", "Lateral derecho") ;
		lista.agregarJugador(j15);
		j15.addPosicionAsignada("Lateral derecho");

		return lista ;
	}
}