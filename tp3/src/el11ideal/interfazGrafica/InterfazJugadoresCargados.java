package el11ideal.interfazGrafica;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import el11ideal.Jugador;
import el11ideal.Jugadores;

import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;

public class InterfazJugadoresCargados extends JFrame {

	private JPanel contentPane;

	public InterfazJugadoresCargados(ArrayList<Jugador> jugadoreslista) {
		
		Jugadores jugs =new InterfazCargaDeDatos().jugadores;
		jugadoreslista= jugs.getListaJugadores();
		setTitle("Mundial Rusia 2018");
		setIconImage(Toolkit.getDefaultToolkit().getImage(InterfazJugadoresCargados.class.getResource("/Imagenes/copa mundial.jpg")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setForeground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setVisible(false);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.setBounds(33, 238, 89, 23);
		btnVolver.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				new InterfazInicio().setVisible(true);
				dispose();
			}
		});
		contentPane.add(btnVolver);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				System.exit(0);
			}
		});
		btnSalir.setBounds(314, 238, 89, 23);
		contentPane.add(btnSalir);
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(10, 45, 414, 188);
		textArea.setText(ImprimirJugadoresCargados(jugadoreslista));
		contentPane.add(textArea);
	}
		
		public String ImprimirJugadoresCargados (ArrayList< Jugador> jugadores)
		{
			String nombre = "";
			for (Jugador i : jugadores)
			{
				nombre = nombre + "\n" + " Nombre: "+ i.getNombre()+" Nacionalidad: "+ i.getNacionalidad() +" Posicion: "+ i.getPosicion();
			}
			return nombre;
		}
		
	}
