package el11ideal.interfazGrafica;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import el11ideal.Jugadores;
import el11ideal.Serializar; 
import java.awt.Toolkit;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import java.awt.Color;

@SuppressWarnings("static-access") 
public class InterfazCargaDeDatos extends JFrame 
{
	private static final long serialVersionUID = -2193667596025166295L; 
	InterfazMostrarListaIdeal ListaIdeal;
	public JPanel contentPane;
	private JTextField txtNombreYApellido;
	private JTextField txtPosicion;
	private JTextField txtNacionalidad;
	private JTextField txtLabelGoles;
	private JTextField txtFaltas;
	private JTextField txtTarjetas;
	private JTextField txtPartidos;
	private JTextField textNombreApellido;
	private JTextField textNacionalidad;
	private JTextField textGoles;
	private JTextField textFaltas;
	private JTextField textTarjetas;
	private JTextField textPartidos;
	private JTextField txtPuntaje;
	private JTextField textPuntaje;
	private JButton btnVerLista;

	Jugadores jugadores = new Jugadores ( );

	public InterfazCargaDeDatos( )  
	{
		cargarJugadores () ;

		getContentPane().setBackground(Color.WHITE);
		setTitle("Mundial Rusia 2018");
		setIconImage(Toolkit.getDefaultToolkit().getImage(InterfazCargaDeDatos.class.getResource("/Imagenes/copa mundial.jpg")));
		getContentPane().setLayout(null);
		setVisible(false);
		setSize(468,331);

		JLabel lblCargaLosDatos = new JLabel("Carga los datos del jugador");
		lblCargaLosDatos.setFont(new Font("Mongolian Baiti", Font.PLAIN, 30));
		lblCargaLosDatos.setBounds(53, 0, 340, 52);
		getContentPane().add(lblCargaLosDatos);

		JLabel lblParaModificarSu = new JLabel("para modificar su coheficiente");
		lblParaModificarSu.setFont(new Font("Mongolian Baiti", Font.PLAIN, 30));
		lblParaModificarSu.setBounds(36, 38, 370, 36);
		getContentPane().add(lblParaModificarSu);

		txtNombreYApellido = new JTextField();
		txtNombreYApellido.setEditable(false);
		txtNombreYApellido.setHorizontalAlignment(SwingConstants.CENTER);
		txtNombreYApellido.setText("Nombre y apellido");
		txtNombreYApellido.setBounds(20, 85, 141, 20);
		getContentPane().add(txtNombreYApellido);
		txtNombreYApellido.setColumns(10);

		txtPosicion = new JTextField();
		txtPosicion.setEditable(false);
		txtPosicion.setHorizontalAlignment(SwingConstants.CENTER);
		txtPosicion.setText("Nacionalidad");
		txtPosicion.setBounds(20, 131, 141, 20);
		getContentPane().add(txtPosicion);
		txtPosicion.setColumns(10);

		txtNacionalidad = new JTextField();
		txtNacionalidad.setEditable(false);
		txtNacionalidad.setHorizontalAlignment(SwingConstants.CENTER);
		txtNacionalidad.setText("Posici\u00F3n");
		txtNacionalidad.setBounds(20, 176, 141, 20);
		getContentPane().add(txtNacionalidad);
		txtNacionalidad.setColumns(10);

		txtLabelGoles = new JTextField();
		txtLabelGoles.setEditable(false);
		txtLabelGoles.setHorizontalAlignment(SwingConstants.CENTER);
		txtLabelGoles.setText("Cantidad de goles");
		txtLabelGoles.setBounds(215, 85, 97, 20);
		getContentPane().add(txtLabelGoles);
		txtLabelGoles.setColumns(10);

		txtFaltas = new JTextField();
		txtFaltas.setEditable(false);
		txtFaltas.setHorizontalAlignment(SwingConstants.CENTER);
		txtFaltas.setText("Faltas");
		txtFaltas.setBounds(215, 116, 97, 20);
		getContentPane().add(txtFaltas);
		txtFaltas.setColumns(10);

		txtTarjetas = new JTextField();
		txtTarjetas.setEditable(false);
		txtTarjetas.setHorizontalAlignment(SwingConstants.CENTER);
		txtTarjetas.setText("Tarjetas");
		txtTarjetas.setBounds(215, 147, 97, 20);
		getContentPane().add(txtTarjetas);
		txtTarjetas.setColumns(10);

		txtPartidos = new JTextField();
		txtPartidos.setEditable(false);
		txtPartidos.setHorizontalAlignment(SwingConstants.CENTER);
		txtPartidos.setText("Partidos");
		txtPartidos.setBounds(215, 176, 97, 20);
		getContentPane().add(txtPartidos);
		txtPartidos.setColumns(10);

		txtPuntaje = new JTextField();
		txtPuntaje.setText("Puntaje");
		txtPuntaje.setHorizontalAlignment(SwingConstants.CENTER);
		txtPuntaje.setEditable(false);
		txtPuntaje.setColumns(10);
		txtPuntaje.setBounds(215, 206, 97, 20);
		getContentPane().add(txtPuntaje);

		JButton btnCancelar = new JButton("Salir");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				System.exit(getDefaultCloseOperation());
			}
		});
		btnCancelar.setBounds(341, 237, 89, 23);
		getContentPane().add(btnCancelar);

		JButton btnCargar = new JButton("Cargar");
		btnCargar.setBounds(116, 237, 89, 23);
		getContentPane().add(btnCargar);

		JButton btnVolver = new JButton("Volver");
		btnVolver.setBounds(17, 237, 89, 23);
		getContentPane().add(btnVolver);
		btnVolver.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				new InterfazInicio().setVisible(true);
				dispose();
			}
		});

		textNombreApellido = new JTextField();
		textNombreApellido.setHorizontalAlignment(SwingConstants.CENTER);
		textNombreApellido.setColumns(10);
		textNombreApellido.setBounds(20, 104, 141, 20);
		getContentPane().add(textNombreApellido);

		textNacionalidad = new JTextField();
		textNacionalidad.setHorizontalAlignment(SwingConstants.CENTER);
		textNacionalidad.setColumns(10);
		textNacionalidad.setBounds(20, 149, 141, 20);
		getContentPane().add(textNacionalidad);

		textGoles = new JTextField();
		textGoles.setHorizontalAlignment(SwingConstants.CENTER);
		textGoles.setColumns(10);
		textGoles.setBounds(309, 85, 97, 20);
		getContentPane().add(textGoles);

		textFaltas = new JTextField();
		textFaltas.setHorizontalAlignment(SwingConstants.CENTER);
		textFaltas.setColumns(10);
		textFaltas.setBounds(309, 116, 97, 20);
		getContentPane().add(textFaltas);

		textTarjetas = new JTextField();
		textTarjetas.setHorizontalAlignment(SwingConstants.CENTER);
		textTarjetas.setColumns(10);
		textTarjetas.setBounds(309, 147, 97, 20);
		getContentPane().add(textTarjetas);

		textPartidos = new JTextField();
		textPartidos.setHorizontalAlignment(SwingConstants.CENTER);
		textPartidos.setColumns(10);
		textPartidos.setBounds(309, 176, 97, 20);
		getContentPane().add(textPartidos);

		textPuntaje = new JTextField();
		textPuntaje.setHorizontalAlignment(SwingConstants.CENTER);
		textPuntaje.setColumns(10);
		textPuntaje.setBounds(309, 206, 97, 20);
		getContentPane().add(textPuntaje);

		JComboBox<Object> Posiciones = new JComboBox<Object>();
		Posiciones.setModel(new DefaultComboBoxModel<Object>(new String[] {"Arquero", "Lateral izquierdo", "Central izquierdo",
				"Central derecho","Lateral derecho", "Volante izquierdo", "Volante central", "Volante derecho", "Puntero izquierdo",
				"Centro delantero", "Puntero derecho"}));
		Posiciones.setBounds(20, 196, 141, 20);
		getContentPane().add(Posiciones);

		btnVerLista = new JButton("Ver equipo");
		btnVerLista.setBounds(215, 237, 116, 23);
		btnVerLista.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{				
				new InterfazMostrarListaIdeal(jugadores).setVisible(true);
				dispose();
			}
		});
		getContentPane().add(btnVerLista);

		JLabel lblNewLabel = new JLabel(""); 
		lblNewLabel.setBounds(0, 0, 452, 292);
		ImageIcon imagen1= new ImageIcon(getClass().getResource("/Imagenes/Mascota1.gif"));
		Icon fondo = new ImageIcon(imagen1.getImage().getScaledInstance(lblNewLabel.getWidth(), lblNewLabel.getHeight(), Image.SCALE_DEFAULT));
		lblNewLabel.setIcon(fondo);
		this.repaint();
		getContentPane().add(lblNewLabel);

		btnCargar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				String NombreYApellido = textNombreApellido.getText();
				int Goles = Integer.parseInt(textGoles.getText());  
				int Faltas = Integer.parseInt(textFaltas.getText());
				int Tarjetas = Integer.parseInt(textTarjetas.getText());
				int Partidos = Integer.parseInt(textPartidos.getText());
				String Nacionalidad = txtNacionalidad.getText();
				int Puntaje = Integer.parseInt(textPuntaje.getText());
				String Posicion = Posiciones.getSelectedItem().toString();
				jugadores.agregarJugador(textNombreApellido.getText(), Goles, Faltas, Tarjetas, Puntaje, Partidos, textNacionalidad.getText(), Posicion);
				Serializar.serializar(Jugadores.getListaJugadores());    
				Jugadores.agregarJugador(NombreYApellido, Goles, Faltas, Tarjetas, Puntaje, Partidos, Nacionalidad , Posicion);
				new InterfazInicio().setVisible(true);
				dispose();
			}
		});
	}

	private void cargarJugadores() 
	{
		try { Serializar.leerArchivo(jugadores); } 
		catch ( Exception ex ) 
		{
			ex.printStackTrace();
		}

	}

	public JTextField cantidadDeGoles() {
		return textGoles;
	}
}