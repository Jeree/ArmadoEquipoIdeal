package el11ideal.interfazGrafica;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;
import el11ideal.Jugadores;


public class InterfazInicio  extends JFrame
{
	private static final long serialVersionUID = -2193667596025166295L; // Esto es para identificar el archivo a guardarse entre clases ( o algo asi )
	public JPanel contentPane;
	public boolean botonContinuar = false;
	InterfazCargaDeDatos CargarDatos = new InterfazCargaDeDatos();
	InterfazMostrarListaIdeal ListaIdeal; 
	InterfazJugadoresCargados jugadoresCargados;
	JLabel EtiquetaFondo;
	ImageIcon imagen;

	public InterfazInicio() 
	{
		setTitle("Mundial Rusia 2018");
		setIconImage(Toolkit.getDefaultToolkit().getImage(InterfazInicio.class.getResource("/Imagenes/copa mundial.jpg")));
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setVisible(true);
		setSize(532,500);

		JLabel lblBienvenido = new JLabel("El equipo ideal");
		lblBienvenido.setVerticalAlignment(SwingConstants.BOTTOM);
		lblBienvenido.setFont(new Font("Rockwell Extra Bold", Font.PLAIN, 38));
		lblBienvenido.setBounds(79, 11, 414, 41);
		contentPane.add(lblBienvenido);

		JLabel lblTeAnimasA = new JLabel("Te damos la bienvenida al creador del ");
		lblTeAnimasA.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		lblTeAnimasA.setBounds(125, 63, 278, 14);
		contentPane.add(lblTeAnimasA);

		JLabel lblMejorEquipoDel = new JLabel("mejor equipo del mundo.");
		lblMejorEquipoDel.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		lblMejorEquipoDel.setBounds(173, 88, 196, 23);
		contentPane.add(lblMejorEquipoDel);

		JButton btnSalir = new JButton("Salir");
		btnSalir.setBounds(386, 410, 120, 23);
		contentPane.add(btnSalir);
		btnSalir.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				System.exit(0);
			}
		});

		JButton btnComenzar_1 = new JButton("Cargar datos");
		btnComenzar_1.setBounds(10, 410, 120, 23);
		contentPane.add(btnComenzar_1);
		btnComenzar_1.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				CargarDatos.setVisible(true);
				dispose();
			}
		});

		JButton VerIdeal = new JButton("Ver equipo");
		VerIdeal.setBounds(136, 410, 120, 23);
		contentPane.add(VerIdeal);
		VerIdeal.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				Jugadores jugs = new InterfazCargaDeDatos( ).jugadores ;
				ListaIdeal = new InterfazMostrarListaIdeal( jugs );
				ListaIdeal.setVisible(true);
				dispose();
			}
		});

		JButton btnVerCargados = new JButton("Ver cargados");
		btnVerCargados.setBounds(260, 410, 120, 23);
		contentPane.add(btnVerCargados);
		btnVerCargados.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				Jugadores jugs = new InterfazCargaDeDatos( ).jugadores ;
				jugadoresCargados = new InterfazJugadoresCargados( Jugadores.getListaJugadores() );
				jugadoresCargados.setVisible(true);
				dispose();
			}
		});
		contentPane.add(btnVerCargados);
		
		JLabel label = new JLabel("");
		label.setBounds(10, 114, 496, 277);
		ImageIcon imagen1= new ImageIcon(getClass().getResource("/Imagenes/Mundial.gif"));
		Icon fondo = new ImageIcon(imagen1.getImage().getScaledInstance(label.getWidth(), label.getHeight(), Image.SCALE_DEFAULT));
		label.setIcon(fondo);
		this.repaint();
		contentPane.add(label);	
		
	}

	public static void main(String[] args) 
	{	
		InterfazInicio inicio= new InterfazInicio();		
		inicio.setVisible(true);
	}
}
