package el11ideal;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class Serializar extends Jugadores implements Serializable   
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2193667596025166295L; // Esto es para identificar el archivo a guardarse entre clases ( o algo asi )

	/**
	 * 
	 */


	public static void serializar ( ArrayList < Jugador > lista) 
	{
		try 
		{
			FileOutputStream archivo = new FileOutputStream("JugadoresGuardados.txt");
			ObjectOutputStream guardado = new ObjectOutputStream(archivo);
			guardado.writeObject(lista);
			guardado.close();
		}
		catch ( Exception ex)
		{
			ex.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	public static Jugadores leerArchivo (Jugadores jugadoresGuardados)
	{
		try 
		{
			FileInputStream archivo = new FileInputStream ( "JugadoresGuardados.txt" );
			ObjectInputStream in = new ObjectInputStream(archivo);
			jugadoresGuardados.setListaJugadores(( ArrayList < Jugador > ) in.readObject()); 
			in.close();
		}
		catch ( Exception ex)
		{
			ex.printStackTrace();
		}
		return jugadoresGuardados ;
	}

}
