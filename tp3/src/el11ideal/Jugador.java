package el11ideal;

import java.io.Serializable;
import java.util.ArrayList;

public class Jugador implements Comparable < Jugador > , Serializable 
{
	private static final long serialVersionUID = -2193667596025166295L; // Esto es para identificar el archivo a guardarse entre clases ( o algo asi )
	private String Nombre;
	private int GolesAnotados;
	private int Faltas;
	private int Tarjetas;
	private int Puntaje;
	private int Partidos;
	private int PromedioPuntaje;
	private int Coeficiente;
	private String Nacionalidad; 
	private String PosicionAsignada;
	private ArrayList <String> Posiciones = new ArrayList <String> ();

	public Jugador(String nombre, int goles, int faltas, int tarjetas,int puntaje,int partidos, String nacionalidad, String posicion)
	{
		this.Nombre= nombre;
		this.GolesAnotados= goles;
		this.Faltas=faltas;
		this.Tarjetas=tarjetas;
		this.Puntaje= puntaje;
		this.Partidos=partidos;
		this.Nacionalidad=nacionalidad;
		this.Posiciones.add(posicion);
		this.coeficiente();
		this.PosicionAsignada=null;
	}

	public void promedioPuntajes()
	{
		this.PromedioPuntaje= Puntaje/Partidos;
	}

	public void coeficiente()
	{
		promedioPuntajes();
		this.Coeficiente = (GolesAnotados*2)-(Faltas/10)-Tarjetas+PromedioPuntaje;
	}

	public int getCoeficiente () 
	{
		return this.Coeficiente;
	}

	public String getNombre ( )
	{
		return this.Nombre;
	}

	//devuelve la cantidad de goles del jugador
	public int getGoles()
	{
		return this.GolesAnotados;
	}

	//devuelve la cantidad de amonestaciones
	public int getAmonestaciones()
	{
		return this.Tarjetas;
	}

	//devuelve la nacionalidad
	public String getNacionalidad()
	{
		return this.Nacionalidad;
	}

	//devuelve la o las posiciones
	public ArrayList <String> getPosicion()
	{
		return this.Posiciones;
	}

	//devuelve la posision que se el asigno en la cancha
	public String getPosicionAsignada()
	{
		return this.PosicionAsignada;
	}

	//asigna una posicion al jugador
	public void addPosicionAsignada(String posicion)
	{
		this.PosicionAsignada=posicion;
	}

	//inicial mente se piede  una sola posicion y despues se le pueden agregar
	public void addPosicion(String posicion)
	{
		this.Posiciones.add(posicion);
	}

	// Metodo CompareTo que se va a usar en el collectio.sort() 
	@Override
	public int compareTo(Jugador otro ) 
	{
		if ( this.getCoeficiente() < otro.getCoeficiente() )
			return +1 ; 

		if ( this.getCoeficiente() > otro.getCoeficiente() )
			return -1 ; 

		return 0;
	}
}
