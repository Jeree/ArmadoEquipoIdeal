package el11ideal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

public class Jugadores implements Serializable   
{
	private static final long serialVersionUID = -2193667596025166295L;
	public static ArrayList <Jugador> _Jugadores;

	public Jugadores()
	{
		_Jugadores = new ArrayList<Jugador>();
	}

	public static void agregarJugador(String nombre, int goles, int faltas, int tarjetas,int puntaje,int partidos, String nacionalidad, String posicion)
	{
		if ( goles < 0 || faltas < 0 || tarjetas < 0 || puntaje < 0 || partidos < 0 )
			throw new IllegalArgumentException();
		Jugador jugador= new Jugador(nombre, goles, faltas, tarjetas, puntaje, partidos, nacionalidad, posicion);
		jugador.coeficiente();
		_Jugadores.add(jugador);
	}

	public void agregarJugador (Jugador j )
	{
		ExisteJugador(j) ;
		_Jugadores.add(j);
	}

	public void quitarJugador(Jugador jugador)
	{
		_Jugadores.remove(jugador);
	}

	public void agreagarPosicion(String nombre, String posicion)
	{
		for(Jugador j : _Jugadores)
		{
			j.getNombre().equals(nombre);
			j.addPosicion(posicion);
		}
	}

	//devuelve la lista ya ordenada
	public static ArrayList<Jugador> getListaJugadores()
	{
		ordenar();
		return _Jugadores;
	}

	public static void ordenar()
	{
		Collections.sort( _Jugadores );

	}

	public static Jugador mejorJugador () 
	{
		return Collections.max( _Jugadores );
	}

	public int getSize () 
	{
		return _Jugadores.size () ;
	}

	public static void ordenarLista(ArrayList <Jugador> jugadores)
	{
		Collections.sort( jugadores);	
	}

	private void ExisteJugador(Jugador otro)
	{
		for ( Jugador j : _Jugadores)
		{
			if ( j.getNombre() == otro.getNombre())
				throw new IllegalArgumentException();
		}
	}

	public void setListaJugadores ( ArrayList <Jugador> listaNueva) 
	{
		_Jugadores = listaNueva ; 
	}

}
