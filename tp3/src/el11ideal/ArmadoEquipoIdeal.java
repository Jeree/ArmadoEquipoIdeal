package el11ideal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

public interface ArmadoEquipoIdeal 
{
	public static ArrayList <Jugador> armadoEquipo(ArrayList <Jugador> _Jugadores)
	{
		ArrayList <Jugador> _EquipoIdeal =new ArrayList <Jugador>();
		int _maxSinGoles=6;
		int _maxAmonestados=5;
		int _maxMismaNacionnalidad=4;
		int jugadoresSinGoles=0;
		int jugadoresAmonestados=0;
		HashMap<String, Integer> Nacionalidades = new HashMap<String, Integer>();
		String _posicionesEquipo[]={"Arquero", "Lateral izquierdo", "Central izquierdo", "Central derecho", "Lateral derecho", "Volante izquierdo", 
				"Volante central", "Volante derecho", "Puntero izquierdo", "Centro delantero", "Puntero derecho"};

		for (String pos:_posicionesEquipo)
		{
			boolean encontrado=false;
			ArrayList <Jugador>	filtrados=filtrarPorPosicion(_Jugadores, pos);
			ordenarLista(filtrados);
			for(Jugador j:filtrados)
			{	
				boolean siguienteJug=false;
				while(encontrado==false && siguienteJug==false)
				{

					if(chequearNacionalidad(Nacionalidades, j.getNacionalidad())!=true)
					{
						if(j.getAmonestaciones()==0)
						{
							if(j.getGoles()>0)
							{
								noExiteJugador(_EquipoIdeal, j);
								addNacionalidad(Nacionalidades, j.getNacionalidad());
								j.addPosicionAsignada(pos);
								_EquipoIdeal.add(j);
								encontrado=true;
							}
							else if(jugadoresSinGoles<_maxSinGoles)
							{
								noExiteJugador(_EquipoIdeal, j);
								jugadoresSinGoles++;
								addNacionalidad(Nacionalidades, j.getNacionalidad());
								j.addPosicionAsignada(pos);
								_EquipoIdeal.add(j);
								encontrado=true;
							}
							else siguienteJug=true;
						}
						else if(jugadoresAmonestados<_maxAmonestados) 
						{
							if(j.getGoles()>0)
							{
								noExiteJugador(_EquipoIdeal, j);
								jugadoresAmonestados++;
								addNacionalidad(Nacionalidades, j.getNacionalidad());
								j.addPosicionAsignada(pos);
								_EquipoIdeal.add(j);
								encontrado=true;
							}
							else if(jugadoresSinGoles<_maxSinGoles)
							{
								noExiteJugador(_EquipoIdeal, j);
								jugadoresAmonestados++;
								jugadoresSinGoles++;
								addNacionalidad(Nacionalidades, j.getNacionalidad());
								j.addPosicionAsignada(pos);
								_EquipoIdeal.add(j);
								encontrado=true;
							}
							else siguienteJug=true;
						}
						else siguienteJug=true;
					}
					else if(cantidadNacianalidad(Nacionalidades,j.getNacionalidad())<_maxMismaNacionnalidad)
					{
						if(j.getAmonestaciones()==0)
						{
							if(j.getGoles()>0)
							{
								noExiteJugador(_EquipoIdeal, j);
								sumarUnoNacionalidad(Nacionalidades, j.getNacionalidad());
								j.addPosicionAsignada(pos);
								_EquipoIdeal.add(j);
								encontrado=true;
							}
							else if(jugadoresSinGoles<_maxSinGoles)
							{
								noExiteJugador(_EquipoIdeal, j);
								sumarUnoNacionalidad(Nacionalidades, j.getNacionalidad());
								jugadoresSinGoles++;
								j.addPosicionAsignada(pos);
								_EquipoIdeal.add(j);	
								encontrado=true;
							}
							else siguienteJug=true;

						}
						else if(jugadoresAmonestados<_maxAmonestados) 
						{
							if(j.getGoles()>0)
							{
								noExiteJugador(_EquipoIdeal, j);
								jugadoresAmonestados++;
								sumarUnoNacionalidad(Nacionalidades, j.getNacionalidad());
								j.addPosicionAsignada(pos);
								_EquipoIdeal.add(j);
								encontrado=true;
							}
							else if(jugadoresSinGoles<_maxSinGoles)
							{	
								noExiteJugador(_EquipoIdeal, j);
								jugadoresAmonestados++;
								jugadoresSinGoles++;
								sumarUnoNacionalidad(Nacionalidades, j.getNacionalidad());
								j.addPosicionAsignada(pos);
								_EquipoIdeal.add(j);
								encontrado=true;
							}	
							else siguienteJug=true;
						}
						else siguienteJug=true;
					}
					else siguienteJug=true;
				}
			}
			
			if(encontrado!=true)
				throw new IllegalArgumentException("no se p�do armar el quipo ya quee no se encontro un jugador para posicion = " + pos);
		}
		return _EquipoIdeal;					
	}

	public static int coeficienteTotal(ArrayList<Jugador> EquipoIdeal)
	{
		int ret=0;
		for(Jugador j:EquipoIdeal)
		{
			ret=ret+j.getCoeficiente();
		}
		return ret;
	}

	//ordena la lista de filtrados por su coeficiente
	public static void ordenarLista(ArrayList<Jugador> filtrados)
	{
		Jugadores.ordenarLista(filtrados);
	}

	//Filtra de al lista original de jugadores dejando solo los que juegan en la posicion pedida
	public static ArrayList<Jugador> filtrarPorPosicion( ArrayList<Jugador> jugadores, String posicion)
	{
		ArrayList <Jugador>	filtrados = new ArrayList <Jugador>();
		for(Jugador j:jugadores)
		{
			if(j.getPosicion().contains(posicion))
				filtrados.add(j);
		}
		return filtrados;
	}

	//se fijaque el jugador no pertenezca al elquipo que se esta armando
	public static boolean noExiteJugador(ArrayList <Jugador> _EquipoIdeal, Jugador j)
	{
		if(!_EquipoIdeal.contains(j))
			return true;	
		throw new IllegalArgumentException("Se quiso agregar una Jugador que ya existe! j = " + j);
	}

	//metodos para controlar las nacionalidades
	public static boolean chequearNacionalidad(HashMap<String, Integer> nacionalidades, String nacionalidad)
	{
		return nacionalidades.containsKey(nacionalidad);
	}

	public static void addNacionalidad(HashMap<String, Integer> nacionalidades, String nacionalidad)
	{
		nacionalidades.put(nacionalidad, 1);
	}

	public static void sumarUnoNacionalidad(HashMap<String, Integer> nacionalidades, String nacionalidad)
	{
		nacionalidades.put(nacionalidad, nacionalidades.get(nacionalidad)+1);
	}

	public static int cantidadNacianalidad(HashMap<String, Integer> nacionalidades, String nacionalidad)
	{
		return nacionalidades.get(nacionalidad).intValue();
	}	

	public static boolean excedeCondicion ( ArrayList <Jugador> jugadores)
	{
		int cantSinGoles = 0 ;
		int cantConTarjetas = 0 ;

		LinkedHashMap<String,Integer> paises =  new LinkedHashMap<String,Integer> () ;

		for ( Jugador j : jugadores)
		{	
			if ( chequearNacionalidad(paises, j.getNacionalidad())) 
				sumarUnoNacionalidad(paises, j.getNacionalidad());
			else
				addNacionalidad(paises, j.getNacionalidad());

			if ( j.getGoles() == 0 )
				cantSinGoles++;
			else if ( j.getAmonestaciones() >= 1 )
				cantConTarjetas++;
		}

		for (Jugador j : jugadores  )
		{
			if ( cantidadNacianalidad(paises, j.getNacionalidad() ) >= 4 )
				return true;

		}

		if ( cantSinGoles >= 6 || cantConTarjetas >= 5  )
			return true ;
		return false;

	}
}